﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IcecreamView;
using System;

[AddComponentMenu("View基础模块/跳转可返回页面")]
public class GameViewModule_LoadBackView : GameViewAbstractModule
{

    [Header("页面跳转事件绑定")]
    public List<BackViewLoadInfo> loadInfos;

    public Action loadAction;

    public override void OnInitView()
    {
        loadInfos.ForEach(p => {
            p.bindEvent(this);
        });
    }

    public override void OnOpenView()
    {
        loadAction = null;
    }

    public override void OnCloseView()
    {
        if (loadAction != null) loadAction();
    }
}

[System.Serializable]
public class BackViewLoadInfo
{
    public Button button;
    public string ViewTable;
    private bool isSingeOpen = false;
    public bool isCloseThis;
    private GameViewAbstractModule m;

    public void bindEvent(GameViewAbstractModule m)
    {
        if (button == null) return;
        this.m = m;
        button.onClick.AddListener(() => {

            ((GameViewModule_LoadBackView)m).loadAction = toBackAction;

            if (isCloseThis)
            {
                m.viewConnector.CloseView();
            }
            else
            {
                toBackAction();
            }
        });
    }

    private void toBackAction() {
        var view = ((GameViewModuleConnector)m.viewConnector.OpenView(ViewTable, false, isSingeOpen)).GetViewModule<GameViewModule_ViewBack>();
        if (view != null)
        {
            view.backViewTable = m.viewConnector.VIEWTABLE;
        }
    }
}
