﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IcecreamView;
using EasyAnimation;


/// <summary>
/// Foldcc
/// 
/// 依赖: 
/// IcecreamView >= V1.2
/// EasyAnimation >= V1.2
/// 说明: UI缓动效果模块，为UI增加缓动效果，如果改UI已存在缓动组件，则控制缓动组件为手动模式，并由该模块控制。
/// </summary>
[AddComponentMenu("View基础模块/页面缓动动画")]
public class GameViewModule_ViewAnim : GameViewAbstractModule {

    public EasyAnimationTemplateMethod easyAnimation;

    public bool OpenStartAnim = true;
    public bool OpenEndAnim = true;

    public override void OnInitView()
    {
        if (easyAnimation == null)
        {
            easyAnimation = gameObject.AddComponent<EasyAnimation_Enlarge>();
            easyAnimation.isReverse = false;
            easyAnimation.easetype = EaseActionMethod.OutBack;
            easyAnimation.animationTime = 0.35f;
        }
        easyAnimation.Stop();
        easyAnimation.isAutoPlay = false;
        easyAnimation.addListener(viewConnector.Continue, PlayActionType.On_End);
    }

    public override void OnOpenView()
    {
        if (!OpenStartAnim) return;
        viewConnector.Await();
        easyAnimation.isReverse = false;
        easyAnimation.rPlay();
    }

    public override void OnCloseView()
    {
        if (!OpenEndAnim) return;
        viewConnector.Await();
        easyAnimation.isReverse = true;
        easyAnimation.rPlay();
    }
}
