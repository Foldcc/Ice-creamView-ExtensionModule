﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IcecreamView;

[AddComponentMenu("View基础模块/页面跳转")]
public class GameViewModule_ViewLoad : GameViewAbstractModule {

    [Header("页面跳转事件绑定")]
    public List<ViewLoadInfo> loadInfos;

    private string ViewTable;
    private bool isSingeOpen;

    public override void OnInitView()
    {
        loadInfos.ForEach(p => {
            p.bindEvent(this);
        });
    }

    public override void OnOpenView()
    {
        ViewTable = null;
    }

    public override void OnCloseView()
    {
        if (ViewTable != null) {
            viewConnector.OpenView(ViewTable , false , isSingeOpen);
        }
    }

    public void setLoadInfo(string viewTable , bool isSinge) {
        ViewTable = viewTable;
        isSingeOpen = isSinge;
    }
}

[System.Serializable]
public class ViewLoadInfo
{
    public Button button;
    public string ViewTable;
    public bool isSingeOpen;
    public bool isCloseThis;

    public void bindEvent(GameViewAbstractModule m)
    {
        if (button)
        {
            button.onClick.AddListener(() => {
                ((GameViewModule_ViewLoad)m).setLoadInfo(ViewTable , isSingeOpen);
                if (isCloseThis) m.viewConnector.CloseView();
                else m.viewConnector.OpenView(ViewTable, false, isSingeOpen);
            });
        }
    }
}
