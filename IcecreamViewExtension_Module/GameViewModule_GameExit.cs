﻿using System.Collections;
using System.Collections.Generic;
using IcecreamView;
using UnityEngine;
using UnityEngine.UI;


public class GameViewModule_GameExit : GameViewAbstractModule
{

    public List<Button> closeBtn;

    public override void OnInitView()
    {
        closeBtn.ForEach(b => {
            if (b)
            {
                b.onClick.AddListener(Application.Quit);
            }
        });
    }
}