﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using IcecreamView;
using UnityEngine;

[AddComponentMenu("View基础模块/页面关闭")]
public class GameViewModule_ViewClose : GameViewAbstractModule {

    public List<Button> closeBtn;

    public override void OnInitView()
    {
        closeBtn.ForEach(b => {
            if (b) {
                b.onClick.AddListener(viewConnector.CloseView);
            }
        });
    }
}
