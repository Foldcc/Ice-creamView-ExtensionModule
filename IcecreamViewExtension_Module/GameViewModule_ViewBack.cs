﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using IcecreamView;
using UnityEngine;

[AddComponentMenu("View基础模块/返回页面")]
public class GameViewModule_ViewBack : GameViewAbstractModule
{
    public List<Button> backBtn;
    private bool defultIsOnce;
    public bool isSingeOpenBackView = true;
    [HideInInspector]
    public string backViewTable;

    public override void OnInitView()
    {
        //defultIsOnce = viewConnector.isOnce;
        //viewConnector.isOnce = false;
        backBtn.ForEach(b => {
            if (b)
            {
                b.onClick.AddListener(viewConnector.CloseView);
            }
        });
    }

    public override void OnCloseView()
    {
        if (backViewTable != null) {
            backView();
        }
    }

    private void backView() {
        //viewConnector.isOnce = defultIsOnce;
        viewConnector.OpenView(backViewTable, false, isSingeOpenBackView);
        backViewTable = null;
    }
}
